package fr.zenika.kata;

import fr.zenika.kata.account.BankAccount;
import fr.zenika.kata.account.BankAccountBuilder;
import fr.zenika.kata.account.NegativeAccountActionException;
import fr.zenika.kata.contact.ContactBuilder;
import fr.zenika.kata.statement.Statements;

import java.time.LocalDate;

public class Main {

    public static void main(String[] args) throws NegativeAccountActionException {

        BankAccount bankAccountSample = new BankAccountBuilder()
                .setInitialAmount(20)
                .setIsActive(true)
                .setHistory(new Statements())
                .setContact(new ContactBuilder()
                        .setFirstName("Rick")
                        .setLastName("sanchez")
                        .setBirthDay(LocalDate.parse("2018-05-05"))
                        .setEmail("Rick@gmail.com")
                        .setPhoneNumber("0606060606")
                        .createContact()).createBankAccount();

        BankAccount bankAccountSample2 = new BankAccountBuilder()
                .setInitialAmount(150)
                .setIsActive(true)
                .setHistory(new Statements())
                .setContact(new ContactBuilder()
                        .setFirstName("Morty")
                        .setLastName("smith")
                        .setBirthDay(LocalDate.parse("2018-05-05"))
                        .setEmail("Morty@gmail.com")
                        .setPhoneNumber("0606060606")
                        .createContact()).createBankAccount();

            bankAccountSample.deposit(10);
            bankAccountSample.withdraw(20);
            bankAccountSample.deposit(20);
            bankAccountSample.deposit(20);
            bankAccountSample.deposit(20);
            bankAccountSample.withdraw(10);
            bankAccountSample.withdraw(10);
            bankAccountSample.transfer(50, bankAccountSample2);
            bankAccountSample.transfer(50, bankAccountSample2);
            bankAccountSample.transfer(50, bankAccountSample2);
            bankAccountSample.transfer(50, bankAccountSample2);
            bankAccountSample2.transfer(100, bankAccountSample);
            bankAccountSample2.transfer(100, bankAccountSample);
            bankAccountSample2.transfer(100, bankAccountSample);
            bankAccountSample2.transfer(100, bankAccountSample);
            bankAccountSample2.transfer(100, bankAccountSample);
            bankAccountSample2.transfer(100, bankAccountSample);
            // print
            bankAccountSample.print();

            bankAccountSample2.print();
            // Negative Ammount Exception
            bankAccountSample.withdraw(-20);
    }

}
