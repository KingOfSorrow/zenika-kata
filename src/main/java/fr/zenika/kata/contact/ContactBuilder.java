package fr.zenika.kata.contact;

import java.time.LocalDate;

public class ContactBuilder {
    private String firstName;
    private String lastName;
    private LocalDate birthDay;
    private String email;
    private String phoneNumber;

    public ContactBuilder setFirstName(String firstName) {
        this.firstName = firstName;
        return this;
    }

    public ContactBuilder setLastName(String lastName) {
        this.lastName = lastName;
        return this;
    }

    public ContactBuilder setBirthDay(LocalDate birthDay) {
        this.birthDay = birthDay;
        return this;
    }

    public ContactBuilder setEmail(String email) {
        this.email = email;
        return this;
    }

    public ContactBuilder setPhoneNumber(String phoneNumber) {
        this.phoneNumber = phoneNumber;
        return this;
    }

    public Contact createContact() {
        return new Contact(firstName, lastName, birthDay, email, phoneNumber);
    }
}