package fr.zenika.kata.statement;

import fr.zenika.kata.utils.StaticValues;
import fr.zenika.kata.operation.Operation;

public class Statement {

	private final Operation operation;
	private final double currentAccountBalance;

	public Statement(Operation operation, double currentBalance) {
		this.operation = operation;
		this.currentAccountBalance = currentBalance;
	}

	@Override
	public int hashCode() {
		return  ((Double)currentAccountBalance).hashCode() + ((operation == null) ? 0 : operation.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Statement other = (Statement) obj;
		if(this.hashCode() == other.hashCode()){
			return true;
		}
		return false;
	}

	@Override
	public String toString() {
		StringBuffer buffer = new StringBuffer();
		buffer.append(StaticValues.STATMENT_LINE);
		buffer.append(operation.getOperationType());
		buffer.append("  ");
		buffer.append(operation.getDate());
		buffer.append("  ");
		buffer.append(operation.getAmount());
		buffer.append("  ");
		buffer.append(currentAccountBalance);
		buffer.append("]");
		return buffer.toString();
	}
}
