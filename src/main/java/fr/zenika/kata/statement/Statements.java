package fr.zenika.kata.statement;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

public class Statements{

	private List<Statement> accountStatements = new ArrayList<Statement>();
	
	public Statements() {
	}
	
	public List<Statement> getAllStatments() {
		return accountStatements;
	}

	public void addStatement(Statement statement) {
		if (statement != null) {
			accountStatements.add(statement);
		}
	}
	
	public void print() {
		accountStatements.stream().limit(10).collect(Collectors.toList()).forEach(System.out::println);
	}
}
