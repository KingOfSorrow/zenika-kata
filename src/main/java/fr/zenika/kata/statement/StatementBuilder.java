package fr.zenika.kata.statement;

import fr.zenika.kata.operation.Operation;
import fr.zenika.kata.utils.StaticValues;

public class StatementBuilder {
    private Operation operation;
    private double currentBalance;

    public StatementBuilder setOperation( Operation operation) {
        if (operation == null) {
            throw new IllegalArgumentException(StaticValues.OPERATION_CANT_BE_NULL);
        }
        this.operation = operation;
        return this;
    }

    public StatementBuilder setCurrentBalance(double currentBalance) {
        this.currentBalance = currentBalance;
        return this;
    }

    public Statement createStatement() {
        return new Statement(operation, currentBalance);
    }
}