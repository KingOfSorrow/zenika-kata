package fr.zenika.kata.utils;

public final class StaticValues {

    public  static  final String STATMENT_LINE = "StatementLine [operation=";
    public static  final String OPERATION_CANT_BE_NULL = "Operation can't be null";
    public static  final String NEGATIVE_AMMOUNT_MESSAGE = "Please supply only positive amounts";
    public static  final String OUT_OF_BALANCE = "Please make sure tour account has enough balance";
}
