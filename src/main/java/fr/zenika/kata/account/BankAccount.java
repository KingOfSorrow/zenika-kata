package fr.zenika.kata.account;

import java.time.Instant;
import java.util.Date;

import fr.zenika.kata.contact.Contact;
import fr.zenika.kata.operation.Operation;
import fr.zenika.kata.operation.OperationBuilder;
import fr.zenika.kata.operation.OperationType;
import fr.zenika.kata.statement.Statement;
import fr.zenika.kata.statement.StatementBuilder;
import fr.zenika.kata.statement.Statements;

public class BankAccount {

	private Contact contact;
    private double accountAmount;
    public Statements history;
    private Boolean isActive;

    public BankAccount(Contact contact, double initialAmount, Statements history, Boolean isActive) {
        this.contact = contact;
        this.accountAmount = initialAmount;
        this.history = history;
        this.isActive = isActive;
    }

    public Statements getHistory() {
        return this.history;
    }

    public void deposit(double deposit) throws NegativeAccountActionException {

        if (deposit < 0) {
            throw new NegativeAccountActionException();
        }

        makeOperation(OperationType.DEPOSIT, deposit);
    }

    public void withdraw(double withdraw) throws NegativeAccountActionException {

        if (withdraw < 0) {
            throw new NegativeAccountActionException();
        }

        makeOperation(OperationType.WITHDRAW, -withdraw);
    }

    public double getBalance() {
        return accountAmount;
    }

    public void print() {
        history.print();
    }

	/**
	 *
	 * @param operationType
	 * @param amount
	 */
	private void makeOperation(OperationType operationType, double amount) {
        accountAmount += amount;
        addToHistory(operationType, Math.abs(amount));
    }

    /**
     * @param amount
     * @param to
     * @return
     */
    public Instant transfer(double amount, BankAccount to) throws NegativeAccountActionException {
        accountAmount = accountAmount - amount;
        Instant timestamp = Instant.now();
        to.receiveTransfer(amount, this, timestamp);
        addToHistory(OperationType.DEPOSIT, amount);
        return timestamp;
    }

    /**
     *
     * @param amount
     * @param senderAccount
     * @param timestamp
     * @throws NegativeAccountActionException
     */
    public void receiveTransfer(double amount, BankAccount senderAccount, Instant timestamp) throws NegativeAccountActionException {
        if(!senderAccount.isActive && senderAccount.getBalance() <= amount){
            throw new NegativeAccountActionException();
        }
        accountAmount = accountAmount + amount;
        addToHistory(OperationType.DEPOSIT, amount);
    }

    /**
     *
     */
    public void blockAccount() {
        this.isActive = true;
    }

	/**
	 *
	 * @param operationType
	 * @param value
	 */
	private void addToHistory(OperationType operationType, double value) {
        Operation operation = new OperationBuilder().setOperationType(operationType).setDate(new Date()).setAmount(value).createOperation();
        Statement statement = new StatementBuilder().setOperation(operation).setCurrentBalance(getBalance()).createStatement();
        history.addStatement(statement);
    }
}