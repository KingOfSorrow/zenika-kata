package fr.zenika.kata.account;

import fr.zenika.kata.contact.Contact;
import fr.zenika.kata.statement.Statements;

public class BankAccountBuilder {
    private double initialAmount;
    private Statements history;
    private Contact contact;
    private Boolean isActive;

    public BankAccountBuilder setContact(Contact contact) {
        this.contact = contact;
        return this;
    }

    public BankAccountBuilder setInitialAmount(double initialAmount) {
        this.initialAmount = initialAmount;
        return this;
    }

    public BankAccountBuilder setHistory(Statements history) {
        this.history = history;
        return this;
    }

    public BankAccountBuilder setIsActive(Boolean isActive) {
        this.isActive = isActive;
        return this;
    }

    public BankAccount createBankAccount() {
        return new BankAccount(contact, initialAmount, history, isActive);
    }
}