package fr.zenika.kata.operation;

import java.util.Date;

public class OperationBuilder {
    private OperationType operationType;
    private Date date;
    private double amount;

    public OperationBuilder setOperationType(OperationType operationType) {
        this.operationType = operationType;
        return this;
    }

    public OperationBuilder setDate(Date date) {
        this.date = date;
        return this;
    }

    public OperationBuilder setAmount(double amount) {
        this.amount = amount;
        return this;
    }

    public Operation createOperation() {
        return new Operation(operationType, date, amount);
    }
}