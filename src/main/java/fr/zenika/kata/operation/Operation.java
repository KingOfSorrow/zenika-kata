package fr.zenika.kata.operation;

import java.util.Date;

public class Operation {
	
	private final OperationType operationType;
	private final Date date;
	private final double amount;
	
	public Operation(OperationType operationType, Date date, double amount) {
		if (operationType == null) {
			throw new IllegalArgumentException("Type can't be null");
		}
		if (date == null) {
			throw new IllegalArgumentException("Date can't be null");
		}
		
		this.operationType = operationType;
		this.date = date;
		this.amount = amount;
	}

	public OperationType getOperationType() {
		return operationType;
	}

	public Date getDate() {
		return date;
	}

	public double getAmount() {
		return amount;
	}
	
	@Override
	public int hashCode() {
		return  ((Double) amount).hashCode()
				+ ((operationType == null) ? 0 : operationType.ordinal())
				+ ((date == null) ? 0 : date.hashCode());
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Operation op = (Operation) obj;
		if(this.hashCode() != op.hashCode()){
			return false;
		}
		return true;
	}

	@Override
	public String toString() {
		return "Operation [type=" + operationType + ", date=" + date + ", amount=" + amount + "]";
	}
}
