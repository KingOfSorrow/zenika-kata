package fr.zenika.kata.operation;

public enum OperationType {
	WITHDRAW,
	DEPOSIT
}
