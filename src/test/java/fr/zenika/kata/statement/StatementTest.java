package fr.zenika.kata.statement;

import java.util.Date;

import fr.zenika.kata.operation.OperationBuilder;
import org.junit.Test;

import fr.zenika.kata.operation.Operation;
import fr.zenika.kata.operation.OperationType;

import static org.assertj.core.api.Assertions.assertThat;

public class StatementTest {

	@Test(expected=IllegalArgumentException.class)
	public void ShouldThrowExceptionWhenOperationIsNull() {
		new StatementBuilder().setOperation(null).setCurrentBalance(1599).createStatement();
	}

	@Test
	public void souldPrintTheExacteSameExpectedResultWhenPrintIsCalled() {

		Operation operation = new OperationBuilder().setOperationType(OperationType.DEPOSIT).setDate(new Date()).setAmount(1000).createOperation();
		Date date = new Date();
		System.out.println(date.toString());
		Statement statement = new StatementBuilder().setOperation(operation).setCurrentBalance(1599).createStatement();
		assertThat(statement.toString()).isEqualTo("StatementLine [operation=DEPOSIT  Mon Jun 22 15:05:01 CEST 2020  1000.0  1599.0]");

		System.out.println(statement.toString());
	}
}
