package fr.zenika.kata.statement;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;


import fr.zenika.kata.operation.Operation;
import fr.zenika.kata.operation.OperationBuilder;
import fr.zenika.kata.operation.OperationType;
import org.junit.Before;
import org.junit.Test;

public class StatementsTest {

	Statements statements;
	
	@Before
	public void init() {

		statements = new Statements();
	}
	
	@Test
	public void theStatementsSouldContainExactlyTheNewAddedValue() {
	
		Operation operation = new OperationBuilder().setOperationType(OperationType.DEPOSIT).setDate(new Date()).setAmount(1000).createOperation();
		Statement statement = new StatementBuilder().setOperation(operation).setCurrentBalance(1599).createStatement();
		
		statements.addStatement(statement);
		
		assertThat(statements.getAllStatments()).containsExactly(statement);
	}
}
