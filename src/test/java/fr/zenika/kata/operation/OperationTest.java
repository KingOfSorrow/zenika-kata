package fr.zenika.kata.operation;

import java.util.Date;

import org.junit.Test;

public class OperationTest {
	
	@Test(expected=IllegalArgumentException.class)
	public void ShouldThrowExceptionWhenDateIsNull() {
		new OperationBuilder().setOperationType(OperationType.DEPOSIT).setDate(null).setAmount(0).createOperation();
	}

	@Test(expected=IllegalArgumentException.class)
	public void ShouldThrowExceptionWhenTypeIsNull() {
		new OperationBuilder().setOperationType(null).setDate(new Date()).setAmount(0).createOperation();
	}

}
