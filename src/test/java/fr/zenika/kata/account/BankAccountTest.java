package fr.zenika.kata.account;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.Date;

import fr.zenika.kata.operation.OperationBuilder;
import fr.zenika.kata.statement.StatementBuilder;
import fr.zenika.kata.statement.Statement;
import org.junit.Before;
import org.junit.Test;

import fr.zenika.kata.operation.Operation;
import fr.zenika.kata.operation.OperationType;
import fr.zenika.kata.statement.Statements;

public class BankAccountTest {

	BankAccount bankAccount;
	
	@Before
	public void init() {

		bankAccount = new BankAccountBuilder().setInitialAmount(0).setHistory(new Statements()).createBankAccount();
	}
	
	@Test
	public void MustHaveExactlyTheNewDepositAmmount() throws NegativeAccountActionException {

		double currentAccountBalance = bankAccount.getBalance();
		bankAccount.deposit(100);
		
		assertThat(bankAccount.getBalance() - currentAccountBalance).isEqualTo(100);
	}
	
	@Test
	public void MustHaveExactlyTheNewWithrawAmmount() throws NegativeAccountActionException {

		double oldBalance = bankAccount.getBalance();
		bankAccount.withdraw(100);
		
		assertThat(oldBalance - bankAccount.getBalance()).isEqualTo(100);
	}

	@Test(expected= NegativeAccountActionException.class)
	public void MustDrowExceptionIfTheDepositAmmountIsNegative() throws NegativeAccountActionException {

		bankAccount.deposit(-100);
	}
	
	@Test(expected= NegativeAccountActionException.class)
	public void MustDrowExceptionIfTheWithrawAmmountIsNegative() throws NegativeAccountActionException {

		bankAccount.withdraw(-100);
	}

	@Test
	public void HistoryMustContainTheWithrawOperationHistory() throws NegativeAccountActionException {

		bankAccount.withdraw(100);

		Operation operation = new OperationBuilder().setOperationType(OperationType.WITHDRAW).setDate(new Date()).setAmount(100).createOperation();
		Statement expectedStatement = new StatementBuilder().setOperation(operation).setCurrentBalance(-100).createStatement();
		assertThat(bankAccount.getHistory().getAllStatments()).containsExactly(expectedStatement);
	}

	@Test
	public void HistoryMustContainTheDepositOperationHistory() throws NegativeAccountActionException {
		
		bankAccount.deposit(1000);
		
		Operation operation = new OperationBuilder().setOperationType(OperationType.DEPOSIT).setDate(new Date()).setAmount(100).createOperation();
		Statement expectedStatement = new StatementBuilder().setOperation(operation).setCurrentBalance(100).createStatement();

		assertThat(bankAccount.getHistory().getAllStatments()).containsExactly(expectedStatement);
	}
	

}
